﻿USE teoria1;


-- 1 numero de coches que ha alquilado el usuarios


-- numero de alquileres
SELECT COUNT(*) nalquileres FROM alquileres a WHERE a.usuario=1;

-- numero de coches

SELECT COUNT(DISTINCT a.coche) ncoches FROM alquileres a WHERE a.usuario=1;

-- numero de alquileres por mes

  SELECT  MONTH(a.fecha) mes,COUNT(*) numero FROM alquileres a GROUP BY MONTH(a.fecha);


 -- numero de hombre y mujeres

  SELECT * FROM usuarios u GROUP BY u.sexo;

  -- numero de alquileres de coches por color

    SELECT c.color, COUNT(*) numero FROM coches c
      JOIN alquileres a 
      ON c.codigoCoche=a.coche
      GROUP BY c.color;
      

-- numero de marcas


  SELECT COUNT(DISTINCT c.marca) FROM coches c;
  
  -- numero de marcas de coches alquilados

    SELECT c.marca, count(*) numero FROM coches c JOIN alquileres a ON c.codigoCoche = a.coche
      GROUP BY c.marca;

    -- otra interpretación

      SELECT COUNT( DISTINCT c.marca) numero FROM coches c JOIN alquileres a ON c.codigoCoche = a.coche;


   
   


  





