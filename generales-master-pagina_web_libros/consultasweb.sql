﻿USE paginaweb;

 -- 1 Titulo de libro u nombre del autor que ha escrito

-- sin join (producto cartesiano)

  SELECT DISTINCT a.nombre_completo,l.titulo FROM autor a,libro l WHERE a.id_autor=l.autor;

 -- con join

SELECT DISTINCT l.titulo,a.nombre_completo FROM libro l JOIN autor a ON a.id_autor=l.autor;


-- 2 nombre del autor y titulo del libro en el que han ayudado

  SELECT DISTINCT a1.nombre_completo,l.titulo 
    FROM libro l 
    JOIN ayuda a ON l.id_libro=a.libro
    JOIN autor a1 ON a1.id_autor=a.autor;

  -- 3 los libros que se ha descargado cada usuario con la fecha de descarga. Listar 
    -- el login, fecha de descarga , id libro

  SELECT DISTINCT f.libro,f.usuario,f.fecha_descarga FROM fechadescarga f;


  -- los libros que se ha descargado cada usuario con la fecha de de descarga. listar el login correo
  --  fechadescarga, titulo del libro

SELECT * 
    FROM libro l JOIN descarga d ON l.id_libro = d.libro 
    JOIN fechadescarga f ON d.libro = f.libro AND d.usuario = f.usuario
    JOIN usuario u ON d.usuario = u.login;

-- 5 el numero de libros que hay

SELECT  COUNT(*) numerolibros FROM libro l;
 

  -- 6 el numero de libros por colección (no hace falta colocar nombre de la coleccion)

    SELECT l.coleccion,count(*) FROM libro l GROUP BY l.coleccion;

    -- 7 la coleccion que tiene mas libros (no hace falta colocar nombre de la colección)

 -- subconsuta c1

SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion;

-- subconsulta c2

  SELECT MAX (c1.nlibros) FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1;


-- consulta final

  SELECT c1.coleccion FROM (
      SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
    )c1
 JOIN (
     SELECT MAX(c1.nlibros) maximo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1
    )c2
    ON c1.nlibros= c2.maximo;

   -- la coleccion que tienes menos libros (no hace falta colocar nombre de la colección)

    -- subconsulta c1 

SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion;

-- subconsulta c2

SELECT MIN(c1.nlibros) minimo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1;


-- subconsulta total con join ejercicio 8


SELECT * FROM 
    (
SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
   
    )c1

  JOIN

  (
  SELECT MIN(c1.nlibros) minimo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1 

  )c2

  ON c1.nlibros=c2.minimo;



-- fina con having

SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion

  HAVING nlibros = (

 SELECT MAX(c1.nlibros) maximo FROM
  (
  
  SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
  )c1
    
  );



-- final con having del ejercicio 8

  SELECT l.coleccion,COUNT(*)nlibros FROM libro l GROUP BY l.coleccion

     HAVING nlibros =(
    
    SELECT MIN(c1.nlibros) minimo FROM

    (

     SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
    )c1
    );



  -- ejercicio 9 con join


 SELECT * FROM coleccion c JOIN 
  (
    SELECT c1.coleccion FROM (
      SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
    )c1
       JOIN (
     SELECT MAX(c1.nlibros) maximo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1
    )c2
    ON c1.nlibros= c2.maximo
  )consulta7
  ON c.id_coleccion=consulta7.coleccion;


-- ejerccio 10 join

  --c1

   SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion;


  -- c2

SELECT Min(c1.nlibros) minimo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1;

-- consulta 8

SELECT  c1.coleccion FROM (
    SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
  )c1
  JOIN
  (
    SELECT Min(c1.nlibros) minimo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1
  )c2
  ON c1.nlibros=c2.minimo;

-- final 10
  SELECT c.nombre FROM coleccion c
    JOIN
    (  
        SELECT  c1.coleccion FROM (
          SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion
        )c1
        JOIN
        (
          SELECT Min(c1.nlibros) minimo FROM  (SELECT l.coleccion,count(*) nlibros FROM libro l GROUP BY l.coleccion) c1
        )c2
        ON c1.nlibros=c2.minimo
    
    )consulta8

    ON
    c.id_coleccion=consulta8.coleccion;


  -- ejercicio 11

    -- c1 
     
  SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro ;

  -- c2

 SELECT MAX(c1.nlibros) maximo FROM  (SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro) c1;


  -- consulta11

    
 SELECT * FROM  
    (
       SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro     
    )c1

  JOIN
    ( 
  SELECT MAX(c1.nlibros) maximo FROM  (SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro) c1
    )c2

   ON c1.ndescargas=c2.maximo;



 -- final ejercicio 11

   SELECT f.libro FROM fechadescarga f

    JOIN (

 (
       SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro     
    )c1

  JOIN
    ( 
  SELECT MAX(c1.nlibros) maximo FROM  (SELECT  f.libro ,count(*) ndescargas FROM  fechadescarga f GROUP BY f.libro) c1
    )c2
 ON
    c.id_coleccion=consulta8.coleccion;
      ON c1.ndescargas=c2.maximo

       )consulta11

     ON
     
    
    


 -- ejercicio 12


    -- c1

    SELECT f.usario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario;

  -- c2
    
     SELECT MAX (c1.ndescargas) maximo FROM
      (
     SELECT f.usario, COUNT(*) ndescargas FROM fechadescarga f GROUP BY f.usuario
      
      
      )c1;

    -- ejercicio 13

      -- c1

    SELECT COUNT(*) ndescargas FROM fechadescarga f WHERE f.usuario='Adam3';

    -- c2

         SELECT f.usuario, COUNT(*) nlibros FROM fechadescarga f GROUP BY f.usuario
          HAVING nlibros>
         
         (
               SELECT COUNT(*) FROM fechadescarga f WHERE f.usuario='Adam3'
                
                
          );

 -- ejercicio 14

  -- c1

     SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga));

    -- c2
      SELECT MAX(c1.ndescargas)maximo FROM (
        
         SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga))
        ) c1;

      -- final con join

        SELECT c1.mes FROM 
          (
            SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga))
          ) c1
          JOIN 
          (
              SELECT MAX(c1.ndescargas) maximo FROM (
            
             SELECT MONTH(f.fecha_descarga) mes, COUNT(*)ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga))
            ) c1
          ) c2
          ON c2.maximo=c1.ndescargas;


        -- final con having


      SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga))
 
     HAVING ndescargas=
 ( 
 
         SELECT MAX(c1.ndescargas)maximo FROM (
        
         SELECT MONTH(f.fecha_descarga) mes, COUNT(*) ndescargas FROM fechadescarga f GROUP BY (MONTH(f.fecha_descarga))
        ) c1

);






  

    







     
























  

  
  


  









