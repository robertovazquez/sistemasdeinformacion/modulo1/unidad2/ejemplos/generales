﻿DROP DATABASE IF EXISTS paginaWeb;
CREATE DATABASE paginaWeb;
USE paginaWeb;

-- creacion de tablas de la pagina

CREATE OR REPLACE TABLE libro(
  id_libro int AUTO_INCREMENT,
  titulo varchar(50),
  coleccion int,
  autor int,
  PRIMARY KEY (id_libro)
);

CREATE OR REPLACE TABLE autor(
  id_autor int AUTO_INCREMENT,
  nombre_completo varchar (50),
  fecha_nacimiento date,
  PRIMARY KEY (id_autor)
  );

CREATE OR REPLACE TABLE usuario(
  login varchar(20),
  contraseña varchar(10),
  email varchar(20),
  fecha_alta date,
  PRIMARY KEY (login)
  );

CREATE OR REPLACE TABLE tipo(
  id_tipo int AUTO_INCREMENT,
  descripcion varchar(10),
  PRIMARY KEY (id_tipo)
  );

CREATE OR REPLACE TABLE ayuda(
  libro int,
  autor int,
  PRIMARY KEY (libro,autor)
  );

CREATE OR REPLACE TABLE tiene(
  libro int,
  tipo int,
  PRIMARY KEY (libro, tipo)
);

CREATE OR REPLACE TABLE descarga(
  libro int,
  usuario varchar(20),
  PRIMARY KEY (libro, usuario)
);

CREATE OR REPLACE TABLE fechaDescarga(
  libro int,
  usuario varchar(20),
  fecha_descarga date,
  PRIMARY KEY (libro, usuario, fecha_descarga)
);

CREATE OR REPLACE TABLE coleccion(
  id_coleccion int AUTO_INCREMENT,
  nombre varchar(20),
  PRIMARY KEY (id_coleccion)
);

-- creacion de foreign keys

ALTER TABLE libro
  ADD CONSTRAINT fklibrocoleccion FOREIGN KEY (coleccion) REFERENCES coleccion(id_coleccion),
  ADD CONSTRAINT fklibroautor FOREIGN KEY (autor) REFERENCES autor(id_autor);

ALTER TABLE ayuda
  ADD CONSTRAINT fkayudalibro FOREIGN KEY (libro) REFERENCES libro(id_libro),
  ADD CONSTRAINT fkayudaautor FOREIGN KEY (autor) REFERENCES autor(id_autor);

ALTER TABLE tiene
  ADD CONSTRAINT fktienelibro FOREIGN KEY (libro) REFERENCES libro(id_libro),
  ADD CONSTRAINT fktienetipo FOREIGN KEY (tipo) REFERENCES tipo(id_tipo);

ALTER TABLE descarga
  ADD CONSTRAINT fkdescargalibro FOREIGN KEY (libro) REFERENCES libro(id_libro),
  ADD CONSTRAINT fkdescargausuario FOREIGN KEY (usuario) REFERENCES usuario(login);

ALTER TABLE fechaDescarga
  ADD CONSTRAINT fkfechadescarga FOREIGN KEY (libro, usuario) REFERENCES descarga (libro, usuario);