﻿ USE concursos;


-- indicar el nombre de los concursantes de burgos


  SELECT DISTINCT c.nombre FROM concursantes c WHERE c.provincia='burgos';




-- Indicar cuantos concursantes hay en burgos


  SELECT DISTINCT count(*) numconcursantes FROM concursantes c WHERE c.provincia='burgos'; 



  -- Indicar cuantos concursantes hay de cada población

     SELECT  c.poblacion ,COUNT(*)  FROM concursantes c GROUP BY c.poblacion;


  -- Indicar las poblaciones que tienen concursantes de menos de 90 kg

    SELECT DISTINCT c.poblacion 
    FROM concursantes c 
    WHERE c.peso<90;



  -- Indicar las poblaciones que tienen mas de 1 concursantes

    SELECT DISTINCT c.poblacion, COUNT(*) 
    FROM concursantes c 
    GROUP BY c.poblacion
      HAVING COUNT(*)>1;



  -- Indicar las poblaciones que tienen mas de 1 concursante de menos de 90 kg


 SELECT DISTINCT c.poblacion
FROM concursantes c 
WHERE c.peso<90  
GROUP BY c.poblacion
HAVING COUNT(*)>1; 


-- listame el nomnbre cuya altura sea mayor de 170

  
SELECT DISTINCT c.nombre FROM concursantes c WHERE c.altura>170;

  -- listame las ponblaciones donde los concursantes pesen menos de 90 kg

 SELECT DISTINCT c.poblacion FROM concursantes c WHERE c.peso<90;
  
  
  -- listame las poblaciones que tengan mas de 5 concursantes con mas de 90 kg

    SELECT DISTINCT c.poblacion FROM concursantes c WHERE c.peso>90 GROUP BY c.poblacion HAVING COUNT(*)>=5; 
  
  
  -- saca la media de altura y altura maxima de los concursantes

   SELECT DISTINCT AVG(c.altura),MAX(c.altura) FROM concursantes c;

  -- media de peso y peso max por poblacion de cantabria

    SELECT DISTINCT AVG(peso),MAX(peso) FROM concursantes c;

  
   
