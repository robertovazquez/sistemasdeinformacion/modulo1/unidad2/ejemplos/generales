﻿USE ciclistas;


/**
  proyeccion
  **/


SELECT DISTINCT c.nombre FROM ciclista AS c;
SELECT DISTINCT c.nombre FROM  ciclista c;
SELECT DISTINCT ciclista.nombre FROM  ciclistas;
SELECT DISTINCT nombre FROM ciclista;


SELECT * FROM 
 ciclista c
 WHERE
 c.edad<30;

SELECT * FROM ciclista c WHERE c.edad<30;

SELECT DISTINCT  c.nombre
 FROM ciclista c WHERE c.edad<30;


-- indicame las edades de los ciclistas de banesto


SELECT DISTINCT c.edad
  FROM ciclista c
  WHERE c.nomequipo= 'banesto';

-- Indicame los nombres de los equipos de los ciclistas que tienen menor de 30

SELECT DISTINCT c.nomequipo
  FROM ciclista c
  WHERE
  c.edad <30;

-- and y or 
  -- listar nombres de los ciclistas cuya edad esta entre los 30 y 32 (inclusive)

  SELECT DISTINCT c.nombre 
    FROM ciclista c
    WHERE c.edad>=30 AND c.edad<=32;

  -- con operadores estendidos


SELECT DISTINCT c.nombre 
    FROM ciclista c
    WHERE c.edad BETWEEN 30 AND 32;



  -- listar los equipos que tengan ciclistas menores de 31 años y que su nombre empieze por m

SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad <31 AND c.nombre LIKE 'M%';

 -- listar los equipos que tengan ciclistas menores de 31 años o que su nombre empieze por m

SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad <31 OR c.nombre LIKE 'M%';


-- listame los ciclistas de banesto y kelme

  SELECT c.dorsal,
         c.nombre,
         c.edad,
         c.nomequipo FROM ciclista c WHERE  c.nomequipo='banesto' OR c.nomequipo='kelme';


  -- operadores extendidos

SELECT c.dorsal,
         c.nombre,
         c.edad,
         c.nomequipo FROM ciclista c WHERE  c.nomequipo IN ('banesto','kelme');

