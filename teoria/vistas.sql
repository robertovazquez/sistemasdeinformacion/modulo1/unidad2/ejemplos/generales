﻿USE teoria1;

--  indicar el nombre de las marcas que se hayan alquilados coches

 -- crear vista

  CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT c.marca FROM alquileres a JOIN coches c ON a.coche = c.codigoCoche;


-- ejecutar la vista 

  SELECT * FROM consulta1;


  -- vamos a realizar la misma pero optimizada


 CREATE OR REPLACE VIEW c1consulta1 AS
   SELECT DISTINCT a.coche FROM alquileres a;
   
-- consulta final

    CREATE OR REPLACE VIEW consulta1 AS

   SELECT DISTINCT c.marca FROM 
    c1consulta1 c1
  JOIN coches c
  ON c1.coche=c.codigoCoche;

  
  
  -- consulta 2

    -- sin optimizar 

    CREATE OR REPLACE VIEW consulta2  AS

SELECT DISTINCT u.nombre FROM alquileres a JOIN usuarios u ON a.usuario = u.codigoUsuario;

    -- consulta 2 optimizada

      -- c2

 CREATE OR REPLACE VIEW c2consulta2 AS
   SELECT DISTINCT a.usuario FROM alquileres a;

-- consulta final


      CREATE OR REPLACE VIEW consulta2 AS

 SELECT DISTINCT u.nombre FROM 
    
        c2consulta2 c2 

      JOIN usuarios u ON c2.usuario=u.codigoUsuario;

 
SELECT * FROM consulta2;


 -- 3 consultua

  -- consulta 3 sin optimizar

 CREATE OR REPLACE VIEW consulta3 AS 
 SELECT  a.codigoAlquiler FROM coches LEFT JOIN alquileres a ON coches.codigoCoche = a.coche WHERE a.coche IS NULL;

-- consulta 3 optimizada

  -- c3
   CREATE OR REPLACE VIEW c3consulta3 AS
   SELECT DISTINCT a.coche FROM alquileres a;

  -- consulta 3 final

    CREATE OR REPLACE VIEW consulta3 AS

 SELECT DISTINCT c.codigoCoche  FROM coches c LEFT JOIN

     c3consulta3 c3

    ON c.codigoCoche = c3.coche WHERE c3.coche IS NULL;  

    
    -- 4 consulta

      CREATE OR REPLACE VIEW consulta4 AS
      SELECT u.codigoUsuario FROM usuarios u LEFT JOIN alquileres a ON u.codigoUsuario = a.usuario WHERE a.usuario IS NULL;

      -- c4

        CREATE OR REPLACE VIEW c4consulta4 AS
        SELECT DISTINCT a.usuario FROM alquileres a;

        -- consulta 4 final


          CREATE OR REPLACE VIEW consulta4 AS

          SELECT u.codigoUsuario FROM usuarios u LEFT JOIN  
    
         c4consulta4 c4
    
     ON u.codigoUsuario=c4.Usuario 
    WHERE c4.Usuario IS NULL;












