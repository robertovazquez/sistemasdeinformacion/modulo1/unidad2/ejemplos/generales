﻿USE teoria1;

-- 1 indicar el nombre de las marcas que se hayan alquilados coches

  -- con sin octimizar


  SELECT DISTINCT c.marca FROM alquileres a JOIN coches c ON a.coche = c.codigoCoche;


  -- consulta octimizada

    -- c1

    SELECT DISTINCT a.coche FROM alquileres a;


    -- consulta final


 SELECT DISTINCT c.marca FROM (
    
  SELECT DISTINCT a.coche FROM alquileres a
    ) c1
  JOIN coches c
  ON c1.coche=c.codigoCoche;


-- 2 nombres de los usuarios que hayan alquilado alguna vez coches

SELECT DISTINCT u.nombre FROM alquileres a JOIN usuarios u ON a.usuario = u.codigoUsuario;



 -- consulta octimizada

  -- c2

   SELECT DISTINCT a.usuario FROM alquileres a;


  -- consulta final

    SELECT DISTINCT u.nombre  FROM usuarios u JOIN (
    
         SELECT DISTINCT a.usuario FROM alquileres a    
      )c2

      ON c2.usuario=u.codigoUsuario;


-- 3 coches que han sido alquilados

   -- sin optimizar

  SELECT  a.codigoAlquiler FROM coches LEFT JOIN alquileres a ON coches.codigoCoche = a.coche WHERE a.coche IS NULL;

  -- optimizada

  -- c1

    SELECT DISTINCT a.coche FROM alquileres a;

 -- final

   SELECT c.codigoCoche FROM coches c LEFT JOIN (
    
    SELECT DISTINCT a.codigoAlquiler FROM alquileres a
    
    )c1
    ON c.codigoCoche= c1.coche
    WHERE c1.coche IS NULL;

  
  -- 4 usuarios que no han alquilado coches

    -- sin optimizar

SELECT u.codigoUsuario FROM usuarios u LEFT JOIN alquileres a ON u.codigoUsuario = a.usuario WHERE a.usuario IS NULL;


   -- optimizada
    
    -- c1
    SELECT DISTINCT a.usuario FROM alquileres a;

    -- consulta final

  SELECT * FROM usuarios u LEFT JOIN  (
    SELECT DISTINCT a.usuario FROM alquileres a

    )c1
     ON u.codigoUsuario=c1.Usuario 
    WHERE c1.Usuario IS NULL;








