﻿USE ciclistas;

-- ciclistas que han ganado etapas

SELECT DISTINCT e.dorsal FROM etapa e;

-- ciclistas que han ganado puertos

  SELECT DISTINCT p.puerto FROM puerto p;

  -- dorsal de los ciclistas que han ganado etapas o puertos

   SELECT DISTINCT e.dorsal FROM etapa e
    UNION
    SELECT DISTINCT p.dorsal FROM puerto p;

  -- union all te saca todos con repetidos

SELECT DISTINCT e.dorsal FROM etapa e
    UNION ALL
    SELECT DISTINCT p.dorsal FROM puerto p;


-- dorsal de los ciclistas que han ganado etapas y puertos

  /*SELECT DISTINCT e.dorsal FROM etapa e
    intersect
    SELECT DISTINCT p.dorsal FROM puerto p;*/

  SELECT dorsalEtapa dorsal FROM
    (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
    JOIN
    (SELECT DISTINCT  p.dorsal dorsalPuerto FROM puerto p) c2

     ON dorsalPuerto= dorsalEtapa;

 
 /* listado de todas las etapas y ciclista que las ha ganado*/
 
    SELECT * FROM ciclista c JOIN etapa e
    ON c.dorsal = e.dorsal;

    /*listado de todos los puertos y ciclistas que la han ganado */

    SELECT * FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal;

    /* si quitas el on y pones where */

    SELECT * FROM puerto p JOIN ciclista c WHERE p.dorsal = c.dorsal;

    /* combinacion interna */

      -- listar todos los ciclistas con todos los datos del equipo al que pertenece

      SELECT * FROM equipo e JOIN ciclista c 
      ON e.nomequipo = c.nomequipo;

     SELECT * FROM equipo e JOIN ciclista c
      USING (nomequipo);

    /*
      producto cartesiano */

      SELECT * FROM  ciclista c ,equipo e;

 -- convertir producto cartsiano en una conbinacion interna


   SELECT * FROM  ciclista c ,equipo e
   WHERE c.nomequipo = e.nomequipo;


   -- listarme los nombres del ciclista y de equipos de aquellos ciclistas que hayan ganado puertos


    SELECT DISTINCT c.nombre,c.nomequipo FROM ciclista c JOIN puerto p ON c.dorsal = p.dorsal;



      


    -- listarme los nombres del ciclista Y del equipo de aquellos ciclistas que hayan ganado etapas

      SELECT DISTINCT c.nombre,c.nomequipo FROM etapa e JOIN ciclista c USING (dorsal);

    -- con  on

       SELECT DISTINCT c.nombre,c.nomequipo FROM etapa e JOIN ciclista c ON e.dorsal = c.dorsal;

      -- c1 (subconsultas)


        SELECT DISTINCT e.dorsal FROM etapa e;

-- completa

  SELECT * FROM ciclista c JOIN ( SELECT DISTINCT e.dorsal FROM etapa e) c1
    USING (dorsal);

  -- quiero saber los ciclistas que han ganado puertos y el numero de puertos que han ganado.Del ciclista quiero saber el dorsal y nombre

    SELECT dorsal,nombre, COUNT(*) numeroPuerto
      FROM
      (SELECT c.dorsal,c.nombre,p.nompuerto FROM puerto.p  JOIN ciclistas c USING
      (dorsal)) c1
      GROUP BY c1.dorsal,c1.nombre;

    SELECT DISTINCT dorsal FROM puerto p JOIN ciclista c USING (dorsal);


    -- cuantas etapas ha ganado cada ciclista


      SELECT e.dorsal,count(*) numeroEtapa FROM etapa e GROUP BY e.dorsal;





      -- dorsal de los ciclistas han ganado mas de 2 etapas


 SELECT e.dorsal,count(*) numeroEtapa FROM etapa e GROUP BY e.dorsal HAVING numeroEtapa>2;


-- nombre del ciclista que tiene mas edad


  


       










    
     

















